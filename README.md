# Setup
```bash
git pull
git submodule update --init # for local and dev
cp docker/docker-compose-env.example.yml docker/docker-compose-env.yml 
```

# Update submodules
```bash
git submodule update
```

# docker-compose files
| Environment | Files |
| ------------------ | --------------- |
| local:       | docker/docker-compose.yml + docker/docker-compose-env.yml + docker/docker-compose-local.yml |
| dev:         | docker/docker-compose.yml + docker/docker-compose-env.yml + docker/docker-compose-dev.yml |
| prod:          | docker/docker-compose.yml + docker/docker-compose-env.yml + docker/docker-compose-prod.yml |

# Deploy
## Local
```bash
bin/build.sh
bin/deploy.sh
# Environment: 0 (local)
# Tag Name: (empty)
# Pull Image?: 0 (not pull)
# Initial Setup: 0 or 1 (select 1 first time only)
```

## Dev
```bash
bin/build.sh
bin/deploy.sh
# Environment: 1 (dev)
# Tag Name: (empty)
# Pull Image?: 0 (not pull)
# Initial Setup: 0 or 1 (select 1 first time only)
```

## Production - Lunch from image
(in local)

```bash
#Build and push image to gitlab 
bin/build.sh
# Tag name: (tag name)
# Push Image?: 1 (push)
```

(in prod)

```bash
#Pull image from gitlab and lunch container from image
bin/deploy.sh
# Environment: 2 (prod)
# Tag Name: (tag name)
# Pull Image?: 1 (pull)
# Initial Setup: 0 or 1 (select 1 first time only)
```
