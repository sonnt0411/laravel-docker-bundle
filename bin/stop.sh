#!/bin/bash

set -eu

cd `dirname $0`
cd ../
ROOT_DIR=`pwd`

read -p "Do you really want to stop the stack? (y/n) > " confirm
if [[ ! "$confirm" = "y" ]]; then
	echo "canceled."
	exit 1
fi

cd docker

docker-compose -p laravel-bundle down
