#!/usr/bin/env bash

docker exec -i $(docker ps -f name=laravel-bundle_$1\\_ --format "{{.ID}}") ${@:2}
