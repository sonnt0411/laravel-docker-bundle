#!/usr/bin/env bash

docker exec $(docker ps -f name=laravel-bundle_$1\\_ --format "{{.ID}}") ${@:2}
