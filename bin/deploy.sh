#!/bin/bash

set -eu

cd `dirname $0`
cd ../
ROOT_DIR=`pwd`

function run_initial_setup {
    ${ROOT_DIR}/bin/exec.sh web-server cp .env.example .env
    ${ROOT_DIR}/bin/exec.sh web-server composer install
    ${ROOT_DIR}/bin/exec.sh web-server php artisan key:generate
    ${ROOT_DIR}/bin/exec.sh web-server php artisan migrate

    return 0
}

echo "=========================="
echo "Docker Bundle - Up Command"
echo "=========================="
echo ""
echo "Select Environment:"
echo "  [0] Local (default)"
echo "  [1] Development"
echo "  [2] Production"
read -p "(0-1 or q to quit) [0] > " env_number
env_number=${env_number:-0}

case ${env_number} in
	0)
		env_name=local
		env_docker_compose_file=docker-compose-local.yml
	;;
	1)
		env_name=dev
		env_docker_compose_file=docker-compose-dev.yml
	;;
	2)
		env_name=prod
		env_docker_compose_file=docker-compose-prod.yml
	;;
	*)
		echo "quit."
		exit 1
	;;
esac
echo "  -> $env_number ($env_name)"
echo ""

read -p "Input Tag Name [latest] > " tag
tag=${tag:-latest}
export TAG=${tag}
echo "  -> $tag"
echo ""

echo "Pull Image?:"
echo "  [0] No (default)"
echo "  [1] Yes"
read -p "(0-1 or q to quit) [0] > " pull_enabled
pull_enabled=${pull_enabled:-0}
echo "  -> $pull_enabled"

if [[ ! ${pull_enabled} = 0 ]] && [[ ! ${pull_enabled} = 1 ]]; then
	echo "quit."
	exit 1
fi

echo ""

echo "Initial Setup:"
echo "  [0] disabled (default)"
echo "  [1] enabled"
read -p "(0-1 or "q" to quit) [0] > " initial_setup_enabled
initial_setup_enabled=${initial_setup_enabled:-0}
if [[ ! ${initial_setup_enabled} = 0 ]] && [[ ! ${initial_setup_enabled} = 1 ]]; then
    echo "quit."
    exit 1
fi
echo "  -> $initial_setup_enabled"
echo ""

cd docker

# pull image
if [[ ${pull_enabled} = 1 ]]; then
	docker-compose pull
fi

# deploy
docker-compose -f docker-compose.yml -f docker-compose-env.yml -f ${env_docker_compose_file} -p laravel-bundle up -d

# setup
if [[ ! ${initial_setup_enabled} = 0 ]]; then
    run_initial_setup
fi
