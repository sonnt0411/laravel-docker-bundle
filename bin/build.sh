#!/bin/bash

set -eu

cd `dirname $0`
cd ../
ROOT_DIR=`pwd`

echo "============================="
echo "Docker Bundle - Build Command"
echo "============================="

read -p "Input Tag Name [latest] > " tag
export TAG=${tag:-latest}
echo ""

echo "Push Image? > "
echo "  [0] not push (default)"
echo "  [1] push"
read -p "(0-1 or q to quit) [0] > " push_enabled
push_enabled=${push_enabled:-0}
if [[ ! ${push_enabled} = 0 ]] && [[ ! ${push_enabled} = 1 ]]; then
	echo "quit."
	exit 1
fi

cd docker

docker-compose build

if [[ ${push_enabled} = 1 ]]; then
	docker-compose push
fi
