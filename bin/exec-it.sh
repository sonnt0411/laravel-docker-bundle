#!/usr/bin/env bash

docker exec -it $(docker ps -f name=laravel-bundle_$1\\_ --format "{{.ID}}") ${@:2}
